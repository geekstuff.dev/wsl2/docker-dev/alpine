#!make

SOURCE_IMAGE ?= alpine
SOURCE_TAG ?= 3.17
TITLE = Docker Dev / Alpine

# used in devcontainer
-include /workspace/common/dev-setup/custom-image.mk

# used in gitlab-ci
ifeq ($(MAKE_LIB),)
include dev-setup/custom-image.mk
endif
